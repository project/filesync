<?php

/**
 * Form builder; Configure file synchronization settings.
 */
function filesync_settings() {
  $form['filesync_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable file synchronization'),
    '#description' => t('Uncheck this box to completely disable Filesync functionality.'),
    '#default_value' => variable_get('filesync_enabled', TRUE),
  );
  $form['filesync_port'] = array(
    '#type' => 'textfield',
    '#title' => t('HTTP port'),
    '#description' => t('Port to use in http connections. If blank, $_SERVER[\'HTTP_PORT\'] will be used.'),
    '#default_value' => variable_get('filesync_port', ''),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['filesync_webheads_source'] = array(
    '#type' => 'radios',
    '#title' => t('Pick the source for webheads IPs'),
    '#options' => array(
      'dns' => t('Resolve IP addresses for the domain.'),
      'reverse_proxy' => t('Use IPs in $conf[\'reverse_proxy_addresses\']'),
      'custom_ips' => t('Specify a list of IPs below'),
    ),
    '#default_value' => variable_get('filesync_webheads_source', NULL),
  );
  if (!variable_get('reverse_proxy', FALSE)) {
    $form['filesync_webheads_source']['reverse_proxy'] = array(
      '#disabled' => TRUE,
      '#description' => t('Option not available. <a href="!url">Reverse proxy</a> is not in use.', array('!url' => 'https://www.drupal.org/node/425990')),
    );
  }
  $form['filesync_webheads_custom_ips'] = array(
    '#type' => 'textarea',
    '#title' => t('Webheads'),
    '#description' => t('Put IP addresses each in a line.'),
    '#disabled' => TRUE,
    '#states' => array(
      'enabled' => array(
        ':input[name="filesync_webheads_source"]' => array('value' => 'custom_ips'),
      ),
    ),
    '#default_value' => variable_get('filesync_webheads_custom_ips', ''),
  );

  return system_settings_form($form);
}

