<?php

/**
 * Implements hook_menu().
 */
function filesync_menu() {
  $items = array();
  $items['admin/config/media/filesync'] = array(
    'title' => 'Filesync',
    'description' => 'Configure file synchronization settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('filesync_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'filesync.admin.inc',
  );
  $items['filesync/%/%/%filesync_file_or_uri'] = array(
    'load arguments' => array(2),
    'page callback' => 'filesync_op',
    'page arguments' => array(1, 2, 3),
    'access callback' => 'filesync_access',
  );
  $items['filesync/image-style-flush/%image_style'] = array(
    'page callback' => 'filesync_op_image_style_flush',
    'page arguments' => array(2),
    'access callback' => 'filesync_access',
  );
  return $items;
}

/**
 * Auto-Loader for %filesync_file_or_uri menu component.
 *
 * Also validates $type.
 */
function filesync_file_or_uri_load($arg, $type) {
  switch ($type) {
    case 'managed':
      $return = file_load($arg);
      break;
    case 'unmanaged':
      $arg = base64_decode(urldecode($arg));
      $return = (file_valid_uri($arg)) ? $arg : FALSE;
      break;
    default:
      $return = FALSE;
  }
  return $return;
}

/**
 * Menu access callback.
 *
 * Checks if the notifier is a webhead.
 */
function filesync_access() {
  $notifier = ip_address();
  $webheads = filesync_webheads();
  return in_array($notifier, $webheads);
}

/**
 * Menu callback for filesync/%/%/%filesync_file_or_uri.
 */
function filesync_op($op, $type, $file) {
  if (!in_array($op, array('cp', 'rm'))) {
    return MENU_NOT_FOUND;
  }

  $uri = ($type == 'managed') ? $file->uri : $file;

  // It only works for stream wrappers that operate on the local filesystem.
  $realpath = drupal_realpath($uri);
  if (!$realpath) {
    $message = 'Unable to resolve local path for %uri';
    $vars = array('%uri' => $uri);
    if ($type == 'managed') {
      $message .= ' (fid: %fid)';
      $vars['%fid'] = $file->fid;
    }
    watchdog('filesync', $message, $vars, WATCHDOG_ERROR);
    drupal_add_http_header('Status', '500 Internal Server Error');
    drupal_exit();
  }

  // Perform the operation.
  switch ($op) {
    case 'cp':
      $path = drupal_substr($realpath, drupal_strlen(DRUPAL_ROOT));
      $url = filesync_url(ip_address(), $path);

      $options = array(
        'headers' => array(
          'Host' => $_SERVER['SERVER_NAME'],
        ),
      );
      httprl_request($url, $options);
      $request = httprl_send_request();

      if ($request[$url]->code == 200) {
        file_put_contents($realpath, $request[$url]->data);
        if ($type == 'managed') {
          touch($realpath, $file->timestamp);
        }
      }
      else {
        $message = 'Failed to retrieve file from %url';
        $vars = array(
          '%url' => $url,
          '%code' => $request[$url]->code,
          '%error' => $request[$url]->error,
        );
        if ($type == 'managed') {
          $message .= ' (fid: %fid)';
          $vars['%fid'] = $file->fid;
        }
        $message .= ' (%code: %error)';
        watchdog('filesync', $message, $vars, WATCHDOG_ERROR);
        drupal_add_http_header('Status', '500 Internal Server Error');
      }
      break;

    case 'rm':
      drupal_unlink($realpath);
      break;
  }

  drupal_exit();
}

/**
 * Menu callback for filesync/image-style-flush/%image_style.
 *
 * @see image_style_flush()
 */
function filesync_op_image_style_flush($style) {
  // Delete the style directory in each registered wrapper.
  $wrappers = file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE);
  foreach ($wrappers as $wrapper => $wrapper_data) {
    if (file_exists($directory = $wrapper . '://styles/' . $style['name'])) {
      file_unmanaged_delete_recursive($directory);
    }
  }

  drupal_exit();
}

/**
 * Implements hook_file_insert().
 */
function filesync_file_insert($file) {
  if (variable_get('filesync_enabled', TRUE)) {
    filesync_notify('cp/managed/' . $file->fid);
  }
}

/**
 * Implements hook_file_delete().
 */
function filesync_file_delete($file) {
  if (variable_get('filesync_enabled', TRUE)) {
    filesync_notify('rm/managed/' . $file->fid);

    // Delete also derivative images.
    $image_styles = image_styles();
    foreach ($image_styles as $style_name => $style_info) {
      $uri = image_style_path($style_name, $file->uri);
      if (file_exists($uri)) {
        $uri_encoded = urlencode(base64_encode($uri));
        filesync_notify('rm/unmanaged/' . $uri_encoded);
      }
    }
  }
}

/**
 * Implements hook_image_imageinfo_cache_save().
 *
 * Notify webheads after derivative images creation.
 */
function filesync_image_imageinfo_cache_save($image, $destination, $return) {
  if ($return && variable_get('filesync_enabled', TRUE)) {
    $uri_encoded = urlencode(base64_encode($destination));
    filesync_notify('cp/unmanaged/' . $uri_encoded);
  }
}

/**
 * Implements hook_image_style_flush().
 */
function filesync_image_style_flush($style) {
  if (variable_get('filesync_enabled', TRUE)) {
    filesync_notify('image-style-flush/' . $style['name']);
  }
}

/**
 * Notify all webheads.
 */
function filesync_notify($command) {
  // Prepare the list of urls of the webheads to notify.
  $webheads = filesync_webheads();
  if (empty($webheads)) {
    watchdog('filesync', 'No webheads found.');
    return;
  }
  $urls = array();
  foreach ($webheads as $webhead) {
    $urls[] = filesync_url($webhead, 'filesync/' . $command);
  }
  // Connect.
  $options = array(
    //'blocking' => FALSE,
    'headers' => array(
      'Host' => $_SERVER['SERVER_NAME'],
    ),
  );
  httprl_request($urls, $options);
  $requests = httprl_send_request();

  // Log failed connections.
  foreach ($requests as $url => $request) {
    if ($request->code != 200) {
      $vars = array(
        '%url' => $url,
        '%code' => $request->code,
        '%error' => $request->error,
      );
      watchdog('filesync', 'Failed to connect to %url (%code: %error)', $vars, WATCHDOG_ERROR);
    }
  }
}

/**
 * Returns an array of all webhead ips but myself.
 */
function filesync_webheads() {
  static $ips = array();

  if (empty($ips)) {
    // Obtain ips.
    $source = variable_get('filesync_webheads_source', '');
    switch ($source) {
      case 'dns':
        $ips = gethostbynamel($_SERVER['SERVER_NAME']);
        break;
      case 'reverse_proxy':
        $ips = variable_get('reverse_proxy_addresses', array());
        break;
      case 'custom_ips':
        $custom_ips = variable_get('filesync_webheads_custom_ips', '');
        if (!empty($custom_ips)) {
          $lines = preg_split("/[\n\r]+/", $custom_ips, -1, PREG_SPLIT_NO_EMPTY);
          foreach ($lines as $line) {
            // Ignore empty lines.
            $line = trim($line);
            if (empty($line)) {
              continue;
            }
            $ips[] = $line;
          }
        }
        break;
    }

    // Exclude self ips.
    $exclude = array('127.0.0.1', $_SERVER['SERVER_ADDR']);
    foreach ($exclude as $ip) {
      $key = array_search($ip, $ips);
      if ($key !== FALSE) {
        unset($ips[$key]);
      }
    }
  }

  return $ips;
}

/**
 * Build url to $ip/$path with proper schema and port.
 */
function filesync_url($ip, $path) {
  $schema = httprl_get_server_schema();
  $port = variable_get('filesync_port', '');
  if (!$port) {
    $port = $_SERVER['SERVER_PORT'];
  }
  $options = array(
    'base_url' => $schema . '://' . $ip . ':' . $port,
    'absolute' => TRUE,
  );
  return url($path, $options);
}

